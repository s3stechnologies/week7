package com.training.s3s.instanceofexample;

public class Parent {
	
	private int age;
	private String name;
	
	
	
//	
//	public Parent(int age, String name) {
//		this.age = age;
//		this.name = name;
//	}
//	
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parent other = (Parent) obj;
		if (age != other.age)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Parent's age is:" + age + ", name is :" + name;
	}
	
	
	
	
	
	

}
