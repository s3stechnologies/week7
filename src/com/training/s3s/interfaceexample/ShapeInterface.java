package com.training.s3s.interfaceexample;

public interface ShapeInterface {
	
	public static final String NAME = "BIWASH";
	public static final int AGE = 20;
	
	
	public void draw(String drawType);  // abstract methods.. methods with no body 
	
	public void color();
	
	public void myName();
	

}
