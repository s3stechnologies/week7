package com.training.s3s.interfaceexample;

public abstract class Animal {
	
	String name;
	int age;
	

//	public Animal(String name, int age) {
//		super();
//		this.name = name;
//		this.age = age;
//	}

	public abstract void printName();

	public void makeNoise() {
		System.out.println("making noise");
	}
	
	
}
