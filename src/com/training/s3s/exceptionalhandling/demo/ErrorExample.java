package com.training.s3s.exceptionalhandling.demo;

public class ErrorExample {

	
	public void method1() {
		this.method2();
	}
	
	public void method2() {
		this.method1();
	}
	
	public static void main(String[] args) {
		
		
		ErrorExample obj = new ErrorExample();
		obj.method1();

	}

}
