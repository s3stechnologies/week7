package com.training.s3s.exceptionalhandling.demo;

public class MyException extends Exception{
	
	
	public MyException() {
		super();
	}
	
	public MyException(String message) {
		super(message);
		
	}
	
	public MyException(String message, Throwable message2) {
		super(message,message2);
		
	}

}
