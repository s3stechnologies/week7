package com.training.s3s.exceptionalhandling.demo;

import java.io.IOException;

class A{
	
	
	public static void isUsingDivideMethod(int a , int b) throws Exception  {
		
	try {
		
		ExceptionHandlingDemo.divideByZeroExample(a, b);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		throw new Exception("exception is thrown");
	}
	
	int sum = 10+20;
		
	System.out.println("Imp stuff to be run!");
	
	}
	
	
	
}


class ExceptionHandlingDemo {

	
	
	//throw
	//throws
	
	
	public  static void divideByZeroExample(int a, int b) throws Exception {
		
		try {
			
			int divideExe = a/b;
			
			//for(int b : a) {
			//	System.out.println(arrayExample[10]);
			//}
		
			//
			// any code here after the exception is thrown in catch won't be excecuted
			// code
			
		}catch(Exception exe) {
			
			throw new Exception("Error is thrown");
			
			//System.out.println("Exception is thrown!");
		
		//	exe.getMessage();
		//	exe.getStackTrace();
		//	exe.printStackTrace();
			
		}
//		}catch(ArrayIndexOutOfBoundsException e) {
			
//			System.out.println("ArithmeticException is thrown!");
//			
//		}catch(ArithmeticException e) {
//			
//			System.out.println(" we cannot divide any number by zero!");
//			// code after erro is thrown will be exceuted
//			
	//	finally {
	//		System.out.println("imp stuff to deal with");
	//		System.out.println("Code runs at the bottom as well when we have caught the exception!");
	//	}
		
		
	}
	
	
	public static void main(String[] args){
		
		int a = 20;
		int b = 0;
		
	//	int a[] = {1,2,3,4,5};
		
		
			try {
				A.isUsingDivideMethod(a, b);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			//divideByZeroExample(a);
		
		
		
		

	}

}
